# INFRA 3 - final project

## Cient - front-end application

## AUTHOR DETAILS

Dean Terneu - ACS202
dean.terneu@student.kdg.be
student-number: 0163905-72

## APPLICATION

This application serves as a client, and sends API request to a seperate backend application.
you can:

* upload
* read
* delete 

images from a gcloud bucket

## PORTS

- test locally on port 8080
- backend application needs to be running on 8086
    * [backend](https://gitlab.com/DeanTerneu/infra3-backend)



