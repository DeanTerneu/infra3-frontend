FROM node:14
LABEL author="Dean Terneu"

# Install xdg-utils
RUN apt-get update && apt-get install -y xdg-utils
WORKDIR /app

COPY package*.json ./
RUN npm install

COPY . .

#RUN npm install
#RUN npm install ajv
RUN npm run build

# Expose the port the app runs on
EXPOSE 8087

# Set the PORT environment variable
ENV PORT 8087

CMD ["npm", "start"]
