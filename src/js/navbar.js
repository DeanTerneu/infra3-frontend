const sectionIds = ['section-upload-file', 'section-list-files']

const linkSearchBookings = document.getElementById('link-upload-file')
const linkAddBooking = document.getElementById('link-list-files')

function enableTab(sectionId) {
    const allSections = document.querySelectorAll('body > section')
    for (const section of allSections) {
        if (section.id === sectionId) {
            section.style.display = 'block'
        } else {
            section.style.display = 'none'
        }
    }
}

linkSearchBookings.addEventListener('click', (event) =>
    event.preventDefault() || enableTab(sectionIds[0]))
linkAddBooking.addEventListener('click', (event) =>
    event.preventDefault() || enableTab(sectionIds[1]))
