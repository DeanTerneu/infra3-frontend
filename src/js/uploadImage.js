import {BASE_URL} from './constants.js'
const fileInput = document.getElementById('image-input')
const imagePreview = document.getElementById('image-preview')
const saveButton = document.getElementById('save-btn')
fileInput.addEventListener('change', function() {
    const file = this.files[0]
    if (file) {
        const reader = new FileReader()
        reader.onload = function(e) {
            const img = document.createElement('img')
            img.src = e.target.result
            img.style.maxWidth = '50%'
            imagePreview.innerHTML = ''
            imagePreview.appendChild(img)
            saveButton.style.display = 'block'
        }
        reader.readAsDataURL(file)
    } else {
        console.error('No file selected.')
    }
})

saveButton.addEventListener('click', async function() {
    const file = fileInput.files[0]
    if (file) {
        try {
            const formData = new FormData()

            const filename = file.name.replace(/\s+/g, '_')

            formData.append('file', file, filename)

            const response = await fetch(`${BASE_URL}/api/mediafiles/upload`, {
                method: 'POST',
                body: formData
            })

            if (response.ok) {

                fileInput.value = ''
                imagePreview.style.display = 'none'
                saveButton.style.display = 'none'

                const body = document.getElementsByTagName('body')[0]

                // Change background color to green for a split second
                body.style.backgroundColor = '#28a745' // Green color
                setTimeout(() => {

                    body.style.backgroundColor = '' // Reset background color
                }, 1000) // Change back after 1 second (adjust as needed)

                const imageUrl = await response.text()
                console.log('Image uploaded:', imageUrl)
                // You can handle the uploaded image URL as needed
            } else {
                throw new Error('Failed to upload image')
            }
        } catch (error) {
            console.error('Error uploading image:', error)
        }
    } else {
        console.error('No file selected.')
    }
})
