import {BASE_URL} from './constants.js'

const uploadImageButton = document.getElementById('link-list-files')
const viewImageSection = document.getElementById('section-list-files')

uploadImageButton.addEventListener('click', async function() {
    viewImageSection.style.display = 'block'
    try {
        const response = await fetch(`${BASE_URL}/api/mediafiles`)
        const data = await response.json()

        const imageContainer = document.getElementById('imageContainer')
        imageContainer.innerHTML = ''

        data.forEach((imageUrl, index) => {
            const imgContainer = document.createElement('div')
            imgContainer.classList.add('card')
            imgContainer.id = `image-card-${index}`

            const img = document.createElement('img')
            img.src = imageUrl
            img.classList.add('card-img-top')

            const deleteBtn = document.createElement('button')
            deleteBtn.textContent = 'Delete'
            deleteBtn.classList.add('btn', 'btn-danger', 'btn-sm', 'mx-2', 'my-2')
            deleteBtn.id = `delete-btn-${index}`

            deleteBtn.addEventListener('click', async function() {
                try {
                    const deleteResponse = await fetch(`${BASE_URL}/api/mediafiles/delete`, {
                        method: 'DELETE',
                        headers: {
                            'Content-Type': 'application/json'
                        },
                        body: JSON.stringify({ imageUrl })
                    })
                    if (deleteResponse.ok) {
                        console.log('Image deleted successfully')
                        imgContainer.remove()
                    } else {
                        throw new Error('Failed to delete image')
                    }
                } catch (error) {
                    console.error('Error deleting image:', error)
                }
            })

            const cardBody = document.createElement('div')
            cardBody.classList.add('card-body')

            imgContainer.appendChild(img)
            cardBody.appendChild(deleteBtn)
            imgContainer.appendChild(cardBody)
            imageContainer.appendChild(imgContainer)
        })

    } catch (error) {
        console.error('Error fetching image URLs:', error)
    }
})
