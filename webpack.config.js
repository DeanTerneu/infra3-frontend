import HtmlWebpackPlugin from 'html-webpack-plugin'
import MiniCssExtractPlugin from 'mini-css-extract-plugin'
import path from 'path'
import webpack from 'webpack'

const __dirname = path.dirname(new URL(import.meta.url).pathname)

const config = {
    devtool: 'source-map',
    mode: 'production',
    optimization: {
        minimize: false, // Disable minification to increase bundle size
        splitChunks: false // Disable code splitting to keep everything in one bundle
    },
    performance: {
        maxEntrypointSize: 650000,
        maxAssetSize: 650000
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: path.resolve('src/html/index.html')
        }),
        new MiniCssExtractPlugin(),
        new webpack.DefinePlugin({
            'process.env.REACT_APP_BACKEND_URL': JSON.stringify(process.env.REACT_APP_BACKEND_URL || 'http://localhost:8086')
        })
    ],
    module: {
        rules: [
            {
                test: /\.s?css$/i,
                use: [
                    MiniCssExtractPlugin.loader,
                    'css-loader',
                    'sass-loader'
                ]
            },
            {
                test: /\.(png|svg|jpe?g|gif)$/i,
                type: 'asset'
            },
            {
                test: /\.(woff2?|eot|ttf|otf)$/i,
                type: 'asset'
            }
        ]
    },
    devServer: {
        static: { directory: path.resolve('dist') },
        hot: false, // optional, but don't enable hot _and_ liveReload together
        liveReload: true,
        open: true,
        port: 8080,
        allowedHosts: 'all'
    },
    output: {
        // Clean 'dist' folder before generating new files
        clean: true
    }
}


config.plugins.push(new webpack.NormalModuleReplacementPlugin(/large-file.js/, 'small-file.js'))

export default config